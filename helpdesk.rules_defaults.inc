<?php

/**
 * Implements hook_default_rules_configuration().
 *
 * Define the rule that enrolls users into purchased courses.
 */
function helpdesk_default_rules_configuration() {
  $configs = array();

  $rule = '
{ "helpdesk_send_feedback_to_atrium" : {
    "LABEL" : "Send Feedback to Atrium",
    "PLUGIN" : "reaction rule",
    "WEIGHT" : "-5",
    "OWNER" : "rules",
    "REQUIRES" : [ "helpdesk", "rules", "feedback_reloaded" ],
    "ON" : { "feedback_reloaded_insert" : [] },
    "IF" : [ { "NOT helpdesk_feedback_method_is_email" : [] } ],
    "DO" : [
      { "helpdesk_rules_action_send_to_atrium" : {
          "USING" : {
            "atrium_version" : "1.x",
            "atrium_title" : "[feedback-reloaded:title]",
            "atrium_body" : "Submitted by: [site:current-user] ( \u003Ca href=\u0022[site:url]\/feedback\/[feedback-reloaded:fid]\u0022 target=\u0022_blank\u0022\u003E[site:url]\/feedback\/[feedback-reloaded:fid]\u003C\/a\u003E )\\r\\nScreenshot: \u003Ca href=\u0022[feedback-reloaded:screenshoturl]\u0022 target=\u0022_blank\u0022\u003E[feedback-reloaded:screenshoturl]\u003C\/a\u003E\\r\\nPage URL: \u003Ca href=\u0022[feedback-reloaded:url]\u0022 target=\u0022_blank\u0022\u003E[feedback-reloaded:url]\u003C\/a\u003E\\r\\n\\r\\nMessage:\\r\\n[feedback-reloaded:message]"
          },
          "PROVIDE" : { "server_response" : { "server_response" : "Server response" } }
        }
      }
    ]
  }
}
  ';
  $configs['helpdesk_send_feedback_to_atrium'] = rules_import($rule);

  return $configs;
}

/**
 * Implements hook_default_rules_configuration_alter().
 */
function helpdesk_default_rules_configuration_alter(array &$configs) {
  if (isset($configs['rules_send_feedback_notification'])) {
//    $configs['rules_send_feedback_notification']->condition('helpdesk_feedback_method_is_email');
    $configs['rules_send_feedback_notification']->active = helpdesk_feedback_method_is_email();
  }
}
