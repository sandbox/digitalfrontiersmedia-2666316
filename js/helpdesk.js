(function ($) {

  var detectedData;
  var positionInfo = '';

  Drupal.behaviors.helpdeskKillMenuItem = {
    attach: function (context, settings) {
      $('.helpdesk-report').attr('href', '#');
    }
  };

  Drupal.behaviors.helpdesk = {
    attach: function (context, settings) {

      for (ajax_el in settings.ajax) {
        if (typeof Drupal.ajax[ajax_el] != 'undefined') {
          if (Drupal.ajax[ajax_el].element.form) {
            if (Drupal.ajax[ajax_el].element.form.id === 'feedback-reloaded-form') {
              // Intercept submission and insert diagnostic info before sending
              Drupal.ajax[ajax_el].beforeSubmit = function(formData) {
                for (var i in formData) {
                  if (formData[i].name == 'message') {
                    // Build Diagnostic info
                    var diagnostics = "\r\n\r\n--\r\n\r\nTECHNICAL DETAILS\r\n";
                    diagnostics += detectData();
                    formData[i].value = formData[i].value + diagnostics;
                    formData.push({"name":"files[upload]", "value":"test stuff"});
                    //formData.push({"name":"files[upload]", "Content-Disposition": "form-data;", "filename":"feedback.html", "Content-Type": "text/plain", "value":"data:text/plain;utf-8,test stuff"});
                    //console.log(formData);
                    break;
                  }    
                }
              }
            }
          }
        }
      }

    (function(){
    var helpdesk_path = Drupal.settings.helpdesk.helpdesk_path;
    var p=[],w=window,d=document,e=f=0;p.push('ua='+encodeURIComponent(navigator.userAgent));e|=w.ActiveXObject?1:0;e|=w.opera?2:0;e|=w.chrome?4:0;
    e|='getBoxObjectFor' in d || 'mozInnerScreenX' in w?8:0;e|=('WebKitCSSMatrix' in w||'WebKitPoint' in w||'webkitStorageInfo' in w||'webkitURL' in w)?16:0;
    e|=(e&16&&({}.toString).toString().indexOf("\n")===-1)?32:0;p.push('e='+e);f|='sandbox' in d.createElement('iframe')?1:0;f|='WebSocket' in w?2:0;
    f|=w.Worker?4:0;f|=w.applicationCache?8:0;f|=w.history && history.pushState?16:0;f|=d.documentElement.webkitRequestFullScreen?32:0;f|='FileReader' in w?64:0;
    p.push('f='+f);p.push('r='+Math.random().toString(36).substring(7));p.push('w='+screen.width);p.push('h='+screen.height);var s=d.createElement('script');
    s.src='/' + helpdesk_path + '/js/whichbrowser/detect.js?' + p.join('&');d.getElementsByTagName('head')[0].appendChild(s);})();

    }
  };

  Drupal.behaviors.helpdesk.report = function () {
    getGeolocationData();
    Browsers = new WhichBrowser();
    $('#feedback_button').click();
  }
    
      function detectData() {
        var screenDimensions = 'Screen Dimensions: ' + window.screen.width + 'px x ' + window.screen.height + 'px'; //Screen Resolution Detection
        var windowDimensions = 'Window Dimensions: ' + jQuery(window).width() + 'px x ' + jQuery(window).height() + 'px'; //Viewport Detection
        var screenDensity = 'Screen Density: ' + window.devicePixelRatio; //Pixel Density Detection
        var colorDepth = 'Screen Color Depth: ' + screen.colorDepth + 'bit'; //Color Depth detection
        var cookiesEnabled = ( navigator.cookieEnabled === true ? 'Cookies: Enabled' : 'Cookies: Disabled' );
        if ( typeof swfobject !== 'undefined' ) {
          var playerVersion = swfobject.getFlashPlayerVersion();
          if ( playerVersion.major != '0' ) { //Working?
            var majorVersion = playerVersion.major;
            var flashEnabled = playerVersion['major'] + '.' + playerVersion['minor'] + '.' + playerVersion['release'];
          } else {
            var flashEnabled = 'Not Supported';
          }
        }

        detectedData = 'Referrer: <a href="' + document.referrer + '" target="_blank">' + document.referrer + '</a>\r\n\r\n';

        detectedData += '\r\n';
        detectedData += 'Browser/OS (unless specified, platform is assumed to be laptop/desktop): \r\n';
        detectedData += Browsers + '\r\n\r\n';
        detectedData += screenDimensions + '\r\n';
        detectedData += windowDimensions + '\r\n';
        detectedData += screenDensity + '\r\n';
        detectedData += colorDepth + '\r\n';
        detectedData += cookiesEnabled + '\r\n';

        detectedData += '\r\n';
        detectedData += 'Plugins: \r\n';
        for(var i = 0; i < navigator.plugins.length; i++) {
          detectedData += '    ' + navigator.plugins[i].name + '\r\n'; 
        }
        detectedData += getFlashVersion();

        detectedData += '\r\n';
        detectedData += positionInfo;

        detectedData += '\r\n';
        detectedData += 'Browser Features: ';
        detectedData += JSON.stringify(Modernizr, null, 4) + '\r\n';

        return detectedData;
      } //End detectData()

      function getGeolocationData() {
        var geoOptions = {
          enableHighAccuracy: true,
          timeout: 5000,
          maximumAge: 0
        };

        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(getPosition, geoErrorCallback, geoOptions);
	} else {
          detectedData += "Geolocation: not supported by this browser.\r\n";
        }
      }

      function getPosition(position) {
        positionInfo += 'Latitude: ' + position.coords.latitude + '\r\n';
        positionInfo += 'Longitude: ' + position.coords.longitude + '\r\n';
        // positionInfo += 'Accuracy: ' + position.coords.accuracy + ' meters' + '\r\n';
        // positionInfo += 'Altitude: ' + position.coords.altitude + '\r\n';
        // positionInfo += 'Speed: ' + position.coords.speed + '\r\n';
        positionInfo += '<a href="https://www.google.com/maps/@' + position.coords.latitude + ',' + position.coords.longitude + ',14z" target="_blank">Map</a>\r\n';
      }

      function geoErrorCallback(error) {
          var strMessage = "";
          // Check for known errors
          switch (error.code) {
              case error.PERMISSION_DENIED:
                  strMessage = 'Access to location is turned off.';
                  break;
              case error.POSITION_UNAVAILABLE:
                  strMessage = "Data from location services is currently unavailable.";
                  break;
              case error.TIMEOUT:
                  strMessage = "Location could not be determined within a specified timeout period.";
                  break;
              default:
                  break;
          }
      }

    function getFlashVersion() {
      var flashVersion = 'no check', d, fv = [];
      if (typeof navigator.plugins !== 'undefined' && typeof navigator.plugins["Shockwave Flash"] === "object") {
        d = navigator.plugins["Shockwave Flash"].description;
        if (d && !(typeof navigator.mimeTypes !== 'undefined' && navigator.mimeTypes["application/x-shockwave-flash"] && !navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin)) {
          // navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin indicates whether plug-ins are enabled or disabled in Safari 3+
          d = d.replace(/^.*\s+(\S+\s+\S+$)/, "$1");
          fv[0] = parseInt(d.replace(/^(.*)\..*$/, "$1"), 10);
          fv[1] = parseInt(d.replace(/^.*\.(.*)\s.*$/, "$1"), 10);
          fv[2] = /[a-zA-Z]/.test(d) ? parseInt(d.replace(/^.*[a-zA-Z]+(.*)$/, "$1"), 10) : 0;
          }
      } else if (typeof window.ActiveXObject !== 'undefined') {
          try {
	  var a = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
          if (a) { // a will return null when ActiveX is disabled
              d = a.GetVariable("$version");
              if (d) {
              d = d.split(" ")[1].split(",");
              fv = [parseInt(d[0], 10), parseInt(d[1], 10), parseInt(d[2], 10)];
              }
            }
          }
          catch(e) {}
      }
      if (fv.length) {
          flashVersion = fv[0] + '.' + fv[1] + ' r' + fv[2];
          return 'Flash Version: ' + flashVersion + '\r\n';
      }
    }

})(jQuery);
