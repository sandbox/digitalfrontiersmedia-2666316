Helpdesk Release 7.x-1.2 (July 29, 2015)



-- SUMMARY --

HelpDesk allows non-technical people to provide highly technical bug reports and supply that information to developers via email or Open Atrium 1.x.  Essentially, HelpDesk is a "glue" module, adding some unique features to a set of existing modules working together.



-- DEPENDENCIES --

  • Feedback Reloaded (https://www.drupal.org/project/feedback_reloaded) - Provides fieldable feedback entities and annotated screenshots (via html2canvas or java applet).
  • Web Service Clients (https://www.drupal.org/project/clients) - Provides a REST method for creating issue reports on Open Atrium 1.x sites.
  • Rules (https://www.drupal.org/project/rules) - Provides a user configurable method of controlling how the systems interact.
  • Drupal Core's Help module - Provides a logical and consistent menu location for initiating reports (recommended to be used with Administration Menu, https://www.drupal.org/project/admin_menu).



-- FEATURES --

  For Site Managers
  • Allows managers to create a screenshot of the page where an issue occurred at the very time of occurrence.
  • Allows managers to highlight issues on the page.
  • Allows managers to black out anything they do not want to appear in a report/screenshot for this issue.
  • Allows managers to add "sticky notes" to annotate their screenshot for further issue clarification.
  • Provides a descriptive title of the issue being reported.
  • Provids a description field to add more detailed information about the error/issue in question.
  • Upon submission, provides a confirmation message that the issue was reported to developer(s).
  
  For Developers
  • Sends report via email or Open Atrium 1.x.
  • Links to the screenshot created by the client (stored on the originating site).
  • Links to the client that created the report.
  • Links to the page on the site where the issue occurred.
  • Links to the page just PRIOR to the page on which the issue was observed (Referrer info so developer has a hint of any preceding steps that occurred).
  • Supplies contextual technical information from the incident:
	• Browser/OS info (including version, via WhichBrowser)
	• Screen Dimensions
	• Window Dimensions
	• Screen Density
	• Screen Color Depth
	• Cookies support
	• Plugins installed and running (including version)
	• Latitude - Longitude
	• Supported Browser Features (via Modernizr)
  • Allows Developer to configure which Group and Project a report should be posted under in Open Atrium.



-- INSTALLATION (on each Reporting Site) --

1.  Install dev version of the Feedback Reloaded module (since the dev version is required and drush will not honor dev module version dependencies, it must be specifically downloaded manually).  Using CLI/drush:
  drush dl feedback_reloaded-7.x-1.x-dev
2.  Download HelpDesk.  Using CLI/drush:
  drush dl helpdesk
3.  Patch the dev version of the Feedback Reloaded module.  The provided patch fixes some bugs discovered in Feedback Reloaded (including screenshot transport encoding issues), changes the screenshot format from PNG to JPG, and updates the version of html2canvas used by the module.  To apply the patch, change your directory at the command line to your Drupal root and execute the following (note that path to patch may vary, depending on your installation location for the helpdesk module):
  patch -p1 < sites/all/modules/helpdesk/patches/helpdesk-feedback-reloaded.patch
4.  Enable the HelpDesk module.  Using CLI/drush:
  drush en helpdesk -y



-- INSTALLATION (one-time install on Open Atrium 1.x Site) --

1.  Download the "services_d6" sandbox project (https://www.drupal.org/sandbox/NadimBak/2022341) to your Open Atrium 1.x site.  This sandbox project picks up the dropped 6.x-3.x branch from the official Services module and is a back port to D6 to fix the CSRF security issue that caused it to initially fall off the official Services module maintainers' radar (https://drupal.org/node/2012982, SA-CONTRIB-2013-051).  Since there is no official release available, the project must be downloaded via git from your Open Atrium site's MODULES directory with the following command:
  git clone --branch master https://git.drupal.org/sandbox/NadimBak/2022341.git services_d6
2.  To ensure that services_d6 follows the same standards used by D7, it must be patched to provide the CSRF user token correctly.  Since the patch found in https://www.drupal.org/node/2016501 doesn't really apply correctly here to services_d6, a replacement patch is provided instead.  To apply the patch, change your directory at the command line to your Drupal root and execute the following (note that path to patch may vary, depending on your installation location for the helpdesk module):
  patch -p1 < sites/all/modules/helpdesk/patches/user_token_d7_method_parity.patch
3.  Enable the Services module and the REST Server module.  Using CLI/drush:
  drush en services rest_server -y



-- CONFIGURATION (one-time configuration on Open Atrium 1.x Site) --

1.  Add a Service at /admin/build/services
  a.  Enter a machine name for the endpoint, e.g. "services"
  b.  Choose "REST" from the "Server" dropdown.
  c.  Add the "Path to endpoint", e.g. "services"
  d.  Enable/check "Session authentication" under the "Authentication" option.
2.  Configure the REST server by clicking on the "Edit Server" Operation link of your newly created service.
  a.  Under "Response Formatters", select/enable:
    i.  json
  b.  Under "Request Parsing", select/enable:
    i.  application/json
    ii.  application/x-www-form-urlencoded
    iii.  multipart/form-data
3.  Configure the Resources to be made available by clicking on the "Edit Resources" Operation link of your newly created service.
  a.  Select/enable the following options:
    i.  comment > CRUD operations > Create
    ii.  file > CRUD operations > Create
    iii.  node > CRUD operations > Create
    iv.  node > Relationships > files
    v.  node > Relationships > comments
    vi.  system > Actions > connect
    vii.  user > Actions > login
    viii.  user > Actions > logout
    ix.  user > Actions > token
4.  Add a user named "helpdesk" to the site (using an "info@"-type email address on the developer's domain is probably best here to support future correspondence feature possibilities, e.g. info@digitalfrontiersmedia.com).
5.  Make sure the "helpdesk" user is added as a member to all the groups that it should be able to post to, especially the specific Group/Project being used to track issues for the particular Reporting Site.  Assuming that all standard authenticated users have sufficient permissions on the site to create issues/cases (default for Open Atrium 1.x), then no additional special permissions should be necessary except for possibly those permissions under "Services" relating to getting and saving files, which may be a future feature consideration. 



-- CONFIGURATION (on each Reporting Site) --

1.  If you choose Open Atrium 1.x as your feedback method, configure the Open Atrium Group ID and Project ID at /admin/config/user-interface/helpdesk for the specific Group/Project being used to track issues for this Reporting Site.  An easy way to get this info is to go to the Open Atrium Content administration page, filter by type using either "Group" or "Project", then checking the links for the Group/Project and getting its Node ID number, e.g. http://example.com/node/XXX, where XXX is the Node ID.  The Node ID for a Group is its Group ID; The Node ID for a Project is its Project ID.
2.  Select "Private (Recommended)" under "Screenshot Storage" at /admin/config/workflow/feedback.
3.  Select "Use the html2canvas library" under "Select the method to create the screenshot" at /admin/config/workflow/feedback.
4.  Enable the "Issue HelpDesk reports" permission for any content/site manager and administrator roles for both client and company users.
5.  Enable the "Access feedback form" permission for any content/site manager and administrator roles for both client and company users.
6.  Configure the HelpDesk Open Atrium Connection REST client at /admin/structure/clients:
  a.  Click the "edit" operation link for the "HelpDesk Open Atrium Connection" that should now be appearing there.
  b.  Enter the URL for the Drupal 6 Services endpoint defined on your Open Atrium 1.x site, e.g. http://example.com/services.
  c.  Enter the username and password for the "helpdesk" user created on your Open Atrium 1.x site.
7.  (Optional and Future Feature Possibilities)  Tweak the data sent to Open Atrium by editing the "Send to Atrium using Clients" Action of the "Send Feedback to Atrium" Rule at admin/config/workflow/rules (requires that the Rules UI [rules_admin] module be enabled).



-- TODO --
1.  Change dependencies/merge with other projects?
2.  Change Rule parameter construction to allow more control over data sent to Open Atrium.
3.  Add Open Atrium 2.x support.
4.  Update to D8.



To submit bug reports and feature suggestions, or to track changes: https://www.drupal.org/project/issues/2666316

Current maintainers:
* Stephen Barker - https://www.drupal.org/user/106070